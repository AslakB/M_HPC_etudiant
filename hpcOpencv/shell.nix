let

  _pkgs = import <nixpkgs> { 
    config = {
      packageOverrides = pkgs: {
        opencv3 = pkgs.opencv3.override (import ../opencv3-opts.nix);
      };
    };
  };

in 

  (_pkgs.python3.withPackages ( ps: [
    ps.matplotlib
    ps.numpy
    ps.tkinter
    ps.opencv3
  ])).env


